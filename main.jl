using HTTP, JSON, Dates
using Plots

GQL_URL = "https://wwgql.coinduf.eu/"
HEADER = ["Content-Type" => "application/json"]

"""
effectue les requêtes graphQL
"""
function graphQLrequest(body)
    r = HTTP.request("POST", GQL_URL, HEADER, JSON.json(Dict("graphQL"=>body)) ) 
    return JSON.parse(String(r.body))
end

# exemple de requête
# r = HTTP.request("POST", "https://wwgql.coinduf.eu/", ["Content-Type" => "application/x-www-form-urlencoded"], "graphQL={version}")
# JSON.parse(String(r.body))

# requête du numéro de version
v  = graphQLrequest("{version}")
print("wotwizard version : ")
println(v["data"]["version"])

# requête pour l'évoltion du nombre de membres
print("requête du nombre de membre... ")
m = graphQLrequest("""
{
    membersCount(start:0,end:null)
    {
        number,
        block { utc0 }
    }
    lossCount(start:0,end:null)
    {
        number
    }
}
""")
println("fait")

# extraction des données dans un vecteur
n = map(x -> x["number"], m["data"]["membersCount"])
l = map(x -> x["number"], m["data"]["lossCount"])
t = map(x -> Date.(unix2datetime.(x["block"]["utc0"])), m["data"]["membersCount"])

# choix du backend
plotly() # affichage interactif dans le navigateur
# gr() # affichage rapide en julia

# affichage de la figure
plot(size=(1200,720), thickness_scaling=1.2, legend=:topleft)
plot!(t,n, label="membres actifs", linetype=:steppost)
plot!(t,l, label="comptes ayant perdu le statut de membre", linetype=:steppost)
plot!(t,n+l, label="total", linetype=:steppost)
title!("Évolution du nombre de membres de la ğ1")
xaxis!("temps")
yaxis!("nombre de membres")

# savefig("membre.png")
