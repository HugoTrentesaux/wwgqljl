# script julia pour afficher l'historique du degré d'un noeud

id = graphQLrequest("""
{
	idSearch (with:{hint:"gerard94"}) {
		ids {
			all_certifiersIO {
				hist {
					block {
						utc0
					}
					in
				}
			}
			all_certifiedIO {
				hist {
					block {
						utc0
					}
					in
				}
			}
		}
	}
}
""")

certifier_list = id["data"]["idSearch"]["ids"][1]["all_certifiersIO"]
# certified_list = id["data"]["idSearch"]["ids"][1]["all_certifiedIO"]

times = []
variations = []

for certifier in certifier_list
    for event in certifier["hist"]
        utc0 = event["block"]["utc0"]
        variation = event["in"] ? +1 : -1
        push!(times, utc0)
        push!(variations, variation)
    end
end

# for certified in certified_list
#     for event in certified["hist"]
#         utc0 = event["block"]["utc0"]
#         variation = event["in"] ? +1 : -1
#         push!(times, utc0)
#         push!(variations, variation)
#     end
# end

p = sortperm(times)
times = times[p]
variations = variations[p]
degree = cumsum(variations)

plot(legend=:topleft)
plot!(times, degree, linetype=:steppost, label="HugoTrentesaux")
plot!(times, degree, linetype=:steppost, label="gerard94")
