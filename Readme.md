# wwgqljl

Client GraphQL pour WotWizard expérimental écrit en julia. Prévisualisation disponible à l'adresse https://wwtmp.coinduf.eu/. 

## Installation

```julia
pkg> add HTTP, JSON, Plots # after ] for package mode
```

## Utilisation

```julia
include("./main.jl") # en attendant mieux
```